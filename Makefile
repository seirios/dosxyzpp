mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
override USER_BINDIR = $(dir $(mkfile_path))

include $(EGS_CONFIG)
include $(SPEC_DIR)egspp1.spec
include $(SPEC_DIR)egspp_$(my_machine).conf

# Specify the name of the user code.
# The name of the executable is determined from this variable.
#
USER_CODE = dosxyzpp
DEF_USER = 

# The following can be used to add user macros and mortran subroutines.
# The file(s) specified here are added after egsnrc.macros, machine.macros
# and egs_c_interface2.macros but before any files that have
# executable code.
#
EGSPP_USER_MACROS = dosxyzpp.macros

# Specify from which base class this application is being derived.
# This has the effect of automatically compiling the base application
# class and including it into the list of object files.
#
EGS_BASE_APPLICATION = egs_advanced_application

# Specify the set of mortran sources used.
# Here we simply use the standard set defined in $HEN_HOUSE/specs/egspp1.spec
#
CPP_SOURCES = $(C_ADVANCED_SOURCES)

# Specify here other header files that your user code depends upon.
#
other_dep_user_code = $(ABS_EGSPP)egs_scoring.h
override dep_user_code = $(USER_CODE).cpp array_sizes.h $(common_h_files1) \
        $(EGS_BASE_APPLICATION).hpp \
        $(common_h_files2) $(other_dep_user_code)

override dep_advanced_application = egs_advanced_application.cpp \
        egs_advanced_application.hpp array_sizes.h \
        $(common_h_files1) $(common_h_files2) \
        $(ABS_EGSPP)egs_input.h $(ABS_EGSPP)egs_base_source.h \
        $(ABS_EGSPP)egs_object_factory.h $(ABS_EGSPP)egs_timer.h \
        $(ABS_EGSPP)egs_application.h $(ABS_EGSPP)egs_run_control.h

include $(HEN_HOUSE)makefiles$(DSEP)cpp_makefile

clean_lib:
	$(REMOVE) $(user_lib_objects) $(egs_lib_objects) $(lib_target)

cleanall: clean clean_lib

