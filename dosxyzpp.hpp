#include "egs_advanced_application.h"
#include "egs_scoring.h"

class DOSXYZpp_Application : public EGS_AdvancedApplication {

private:

    EGS_ScoringArray *dose;
    double Etot;
    bool quiet;
    int nreg;

public:

    DOSXYZpp_Application(int,char**);
    ~DOSXYZpp_Application();
    void describeUserCode() const;
    int initScoring();
    int ausgab(int);
    int outputData();
    int readData();
    void resetCounter();
    int addState(istream &);
    void outputResults();
    void getCurrentResult(double &,double &,double &,double &);

    void setInput(string);
    void setHenHouse(string);
    void setEgsHome(string);
    void setPegsFile(string);
    void setInputFile(string);
    void setOutputFile(string);
    void setAppName(string);
    void setAppDir(string);
    void setRunDir(string);
    void setParallel(long,long);
    void setBatch();
    void setQuiet();
    void setSimpleRun();

    double getEtot();
    double getMass(int);
    void getDoseScore(int,double &,double &);
    int getNRegions();
    long getNCase();

    int initGeom();

protected:

    int startNewShower();

};
