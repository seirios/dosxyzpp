#include "egs_interface2.h"
#include "egs_input.h"
#define EXPLICIT_XYZ
#include "geometry/egs_nd_geometry/egs_nd_geometry.h"

#include "dosxyzpp.hpp"

DOSXYZpp_Application::DOSXYZpp_Application(int argc, char *argv[]) : EGS_AdvancedApplication(argc,argv) { 
    quiet = false;
    dose = NULL;
    Etot = 0.0;
    nreg = 0;
}

DOSXYZpp_Application::~DOSXYZpp_Application() {
    if(dose) delete dose;
}

void DOSXYZpp_Application::describeUserCode() const {
    egsInformation(
            "\n               ***************************************************"
            "\n               *                                                 *"
            "\n               *                  DOSXYZpp                       *"
            "\n               *                                                 *"
            "\n               ***************************************************"
            "\n\n");
    egsInformation("This is DOSXYZpp_Application based on\n"
            "      EGS_AdvancedApplication \n\n");
}

int DOSXYZpp_Application::initScoring() {
    nreg = geometry->regions();
    dose = new EGS_ScoringArray(nreg+1);
    dose->reset();

    // set ausgab call to false for everything by default
    for (int i = 0; i < UnknownCall; i++)
        setAusgabCall((AusgabCall)i, false);

    // if we want to calculate dose, we need ausgab being called for
    // BeforeTransport, EgsCut, PegsCut, UserDiscard, ExtraEnergy
    setAusgabCall(BeforeTransport, true);
    setAusgabCall(EgsCut, true);
    setAusgabCall(PegsCut, true);
    setAusgabCall(UserDiscard, true);
    setAusgabCall(ExtraEnergy, true);

    return 0;
}

int DOSXYZpp_Application::ausgab(int iarg) {
    if(iarg <= 4) {
        int np = the_stack->np - 1;
        int ir = the_stack->ir[np] - 1;
        // 0 is outside, 1..nreg are in geometry
        dose->score(ir,the_epcont->edep * the_stack->wt[np]);
    }
    return 0;
}

int DOSXYZpp_Application::outputData() {
    if(quiet == false) {
        int err = EGS_AdvancedApplication::outputData();
        if(err) return err;

        (*data_out) << "  " << Etot << endl;
        dose->storeState(*data_out);
    }
    return 0;
}

int DOSXYZpp_Application::readData() {
    if(quiet == false) {
        int err = EGS_AdvancedApplication::readData();
        if(err) return err;

        (*data_in) >> Etot;
        dose->setState(*data_in);
    }
    return 0;
}

void DOSXYZpp_Application::resetCounter() {
    EGS_AdvancedApplication::resetCounter();
    Etot = 0.0;
    dose->reset();
}

int DOSXYZpp_Application::addState(istream &data) {
    int err = EGS_AdvancedApplication::addState(data);
    if(err) return err;

    double tmp_etot; data >> tmp_etot; Etot += tmp_etot;

    EGS_ScoringArray tmpDose(nreg+1);
    tmpDose.setState(data);
    (*dose) += tmpDose;

    return 0;
}

double dose_result_in_Gy(double res, EGS_I64 ncase, double mass, double fluence) {
    static const double MeV = 1.6021766208E-10;
    return res * (ncase * MeV / (mass * fluence));
}

void DOSXYZpp_Application::outputResults() {
    EGS_XYZGeometry *doseGeometry = (EGS_XYZGeometry*)geometry;
    double *doseValues, *doseErrors;

    if(quiet == false) {
        doseValues = (double*)malloc(nreg * sizeof(double));
        doseErrors = (double*)malloc(nreg * sizeof(double));

        for (int r=0;r<nreg;r++) {
            double e,de;
            dose->currentResult(r+1,e,de);            
            doseErrors[r] = e > 0.0 ? de/e : 1.0;
            doseValues[r] = dose_result_in_Gy(e,current_case,getMass(r),current_case);
        }

        FILE *doseBin = fopen(constructIOFileName(".dose",false).c_str(), "wb");

        int dim[3];
        dim[0] = doseGeometry->getNx();
        dim[1] = doseGeometry->getNy();
        dim[2] = doseGeometry->getNz();
        fwrite(dim, sizeof(int), 3, doseBin);

        fwrite(doseGeometry->getXPositions(), sizeof(double), doseGeometry->getNx() + 1, doseBin);
        fwrite(doseGeometry->getYPositions(), sizeof(double), doseGeometry->getNy() + 1, doseBin);
        fwrite(doseGeometry->getZPositions(), sizeof(double), doseGeometry->getNz() + 1, doseBin);

        fwrite(doseValues, sizeof(double), nreg, doseBin);
        fwrite(doseErrors, sizeof(double), nreg, doseBin);

        fclose(doseBin);

        free(doseValues);
        free(doseErrors);
    }

    egsInformation("\n\n last case = %d Etot = %g\n",(int)current_case,Etot);
    int nvox = geometry->getNRegDir(0);
    int ir = (nvox/2) + (nvox/2) * nvox + (nvox/2) * nvox * nvox + 1;
    double sum,sum2,f,df;
    long ncase = current_case;
    dose->currentScore(ir,sum,sum2);
    sum /= ncase;
    sum2 /= ncase;
    sum2 = sqrt((sum2 - sum*sum)/(ncase - 1));
    sum2 /= sum;
    sum *= 1.602E-10 / getMass(ir);
    f = sum;
    df = 100.0*sum2;
    egsInformation("\n\n dose to center voxel = %g +/- %g %\n",f,df);
}

// Report current dose in center voxel
void DOSXYZpp_Application::getCurrentResult(double &sum, double &sum2,
        double &norm, double &count) {
    int nvox = geometry->getNRegDir(0);
    int ir = (nvox/2) + (nvox/2) * nvox + (nvox/2) * nvox * nvox + 1;

    count = current_case;
    dose->currentScore(ir,sum,sum2);
    norm = dose_result_in_Gy(1.0,count,getMass(ir),current_case);
}

int DOSXYZpp_Application::startNewShower() {
    Etot += p.E * p.wt;

    if( current_case != last_case ) {
        dose->setHistory(current_case);
        last_case = current_case;
    }

    return 0;
}

void DOSXYZpp_Application::setInput(string inputString) {
    input = new EGS_Input;
    input->setContentFromString(inputString);
}

void DOSXYZpp_Application::setHenHouse(string HenHouse) {
    hen_house = HenHouse;
}

void DOSXYZpp_Application::setEgsHome(string EgsHome) {
    egs_home = EgsHome;
}

void DOSXYZpp_Application::setPegsFile(string PegsFile) {
    is_pegsless = false;
    pegs_file = PegsFile;
    abs_pegs_file = PegsFile;
}

void DOSXYZpp_Application::setInputFile(string InpFile) {
    input_file = InpFile;
}

void DOSXYZpp_Application::setOutputFile(string OutFile) {
    output_file = OutFile;
}

void DOSXYZpp_Application::setAppName(string AppName) {
    app_name = AppName;
}

void DOSXYZpp_Application::setAppDir(string AppDir) {
    app_dir = AppDir;
}

void DOSXYZpp_Application::setRunDir(string RunDir) {
    run_dir = RunDir;
}

void DOSXYZpp_Application::setParallel(long npar, long ipar) {
    batch_run = true;
    n_parallel = npar;
    i_parallel = ipar;
}

void DOSXYZpp_Application::setBatch() {
    batch_run = true;
}

void _quiet_info_function(const char *msg, ...) {
}

void DOSXYZpp_Application::setQuiet() {
    quiet = true;
    //egsSetInfoFunction(Information,_quiet_info_function);
    //egsSetInfoFunction(Warning,_quiet_info_function);
    //egsSetInfoFunction(Fatal,_quiet_info_function);
}

void DOSXYZpp_Application::setSimpleRun() {
    simple_run = true;
}

double DOSXYZpp_Application::getEtot() {
    return Etot;
}

/* assumes ir is an inside region (1..nreg) */
double DOSXYZpp_Application::getMass(int ir) {
    /* volume times relative density in region */
    double volume_scaled = geometry->getMass(ir-1);
    /* actual density of medium in region */
    double density = getMediumRho(geometry->medium(ir-1));
    return volume_scaled * density;
}

void DOSXYZpp_Application::getDoseScore(int ir, double &sum, double &sum2) {
    dose->currentScore(ir,sum,sum2);
}

int DOSXYZpp_Application::getNRegions() {
    return geometry->regions();
}

long DOSXYZpp_Application::getNCase() {
    return (long)current_case;
}

int DOSXYZpp_Application::initGeom() {
    return initGeometry();
}

#ifdef BUILD_APP_LIB
APP_LIB(DOSXYZpp_Application);
#else
APP_MAIN(DOSXYZpp_Application);
#endif
